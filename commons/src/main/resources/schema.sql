CREATE TABLE `oauth_client_details`
(
    `client_id`                      varchar(256) NOT NULL,
    `client_secret`                  varchar(256) DEFAULT NULL,
    `scope`                          varchar(256) DEFAULT NULL,
    `authorized_grant_types`         varchar(256) DEFAULT NULL,
    `web_server_redirect_uri`        varchar(256) DEFAULT NULL,
    `access_token_validity_seconds`  int(11)      DEFAULT NULL,
    `refresh_token_validity_seconds` int(11)      DEFAULT NULL,
    `auto_approve_scopes`            varchar(256) DEFAULT NULL,
    PRIMARY KEY (`client_id`)
);

CREATE TABLE `users`
(
  `id`          int NOT NULL AUTO_INCREMENT,
  `uuid`        varchar(256) UNIQUE,
  `username`    varchar(256) UNIQUE,
  `password`    varchar(256),
  PRIMARY KEY (`id`)
);

CREATE TABLE `roles`
(
  `id`          int NOT NULL AUTO_INCREMENT,
  `name`        varchar(256) UNIQUE,
  PRIMARY KEY (`id`)
);

CREATE TABLE `user_roles`
(
  `users`       int NOT NULL,
  `roles`       int NOT NULL,
  PRIMARY KEY (`users`, `roles`),

  CONSTRAINT `constr_user_roles_user_fk`
        FOREIGN KEY (`users`) REFERENCES `users` (`id`)
        ON DELETE CASCADE ON UPDATE CASCADE,

  CONSTRAINT `constr_user_roles_roles_fk`
        FOREIGN KEY (`roles`) REFERENCES `roles` (`id`)
        ON DELETE CASCADE ON UPDATE CASCADE
);