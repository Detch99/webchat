package com.detch.auth.dao;

import com.detch.auth.model.OauthClientDetails;
import org.springframework.data.repository.CrudRepository;

public interface OauthClientDetailsDao extends CrudRepository<OauthClientDetails, String> {}
