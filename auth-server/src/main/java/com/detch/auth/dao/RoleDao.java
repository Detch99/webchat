package com.detch.auth.dao;

import com.detch.auth.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleDao extends CrudRepository<Role, Long> {

    Role findByName(String name);
}
