package com.detch.auth.model;

import com.detch.auth.validation.PasswordMatches;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@PasswordMatches
public class RegistrationDTO {

    @NotNull(message = "Username must be not null!")
    @NotEmpty(message = "Username must be not empty!")
    private String username;

    @NotNull(message = "Password must be not null!")
    @NotEmpty(message = "Password must be not empty!")
    @Length(min = 5, message = "Password must contain at least 5 symbols")
    private String password;

    private String matchingPassword;
}
