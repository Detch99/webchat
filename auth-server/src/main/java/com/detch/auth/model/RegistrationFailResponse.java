package com.detch.auth.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class RegistrationFailResponse extends RegistrationResponse {

    private Map<String, List<String>> errors;
}
