package com.detch.auth.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class RegistrationSuccessResponse extends RegistrationResponse {

    private UserDTO user;

}
