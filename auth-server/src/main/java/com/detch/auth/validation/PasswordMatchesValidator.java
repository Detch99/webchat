package com.detch.auth.validation;

import com.detch.auth.model.RegistrationDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, RegistrationDTO> {

   public void initialize(PasswordMatches constraint) {
   }

   @Override
   public boolean isValid(RegistrationDTO registrationDTO, ConstraintValidatorContext constraintValidatorContext) {
      return Objects.equals(registrationDTO.getPassword(), registrationDTO.getMatchingPassword());
   }
}
