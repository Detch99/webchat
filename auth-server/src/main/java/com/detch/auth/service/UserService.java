package com.detch.auth.service;

import com.detch.auth.dao.RoleDao;
import com.detch.auth.dao.UserDao;
import com.detch.auth.exceptions.UserAlreadyExistsException;
import com.detch.auth.model.Role;
import com.detch.auth.model.User;
import com.detch.auth.model.RegistrationDTO;
import com.detch.auth.model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Service
public class UserService {

    private UserDao userDao;
    private RoleDao roleDao;
    private PasswordEncoder passwordEncoder;

    private static final String DEFAULT_ROLE = "ROLE_USER";

    public UserDTO registerUserAccount(RegistrationDTO registrationDTO) {
        Set<Role> roles = new HashSet<>(Collections.singletonList(roleDao.findByName(DEFAULT_ROLE)));
        return registerUserAccount(registrationDTO, roles);
    }

    public UserDTO registerUserAccount(RegistrationDTO registrationDTO, Set<Role> roles) {
        String username = registrationDTO.getUsername();
        if (userDao.existsByUsername(username)) {
            throw new UserAlreadyExistsException("A user with the name '" + username + "' already exists!");
        }
        String encodedPassword = passwordEncoder.encode(registrationDTO.getPassword());
        User user = new User();
        user.setUsername(username);
        user.setPassword(encodedPassword);
        user.setRoles(roles);
        user.setUuid(UUID.randomUUID().toString());
        userDao.save(user);
        return new UserDTO(user.getUuid(), user.getUsername());
    }

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Autowired
    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
}
