package com.detch.auth.service;

import com.detch.auth.dao.OauthClientDetailsDao;
import com.detch.auth.model.OauthClientDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Service;

import static org.springframework.util.StringUtils.commaDelimitedListToSet;

@Service("oauthClientDetailsImpl")
public class ClientDetailsServiceImpl implements ClientDetailsService {

    private OauthClientDetailsDao oauthClientDetailsDao;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        OauthClientDetails oauthClientDetails = oauthClientDetailsDao
                .findById(clientId)
                .orElseThrow(() -> new ClientRegistrationException("Client not found"));

        BaseClientDetails baseClientDetails = new BaseClientDetails();

        baseClientDetails.setClientId(oauthClientDetails.getClientId());
        baseClientDetails.setScope(commaDelimitedListToSet(oauthClientDetails.getScope()));
        baseClientDetails.setAuthorizedGrantTypes(commaDelimitedListToSet(oauthClientDetails.getAuthorizedGrantTypes()));
        baseClientDetails.setRegisteredRedirectUri(commaDelimitedListToSet(oauthClientDetails.getRedirectUris()));
        baseClientDetails.setAutoApproveScopes(commaDelimitedListToSet(oauthClientDetails.getAutoApproveScopes()));
        baseClientDetails.setClientSecret(oauthClientDetails.getClientSecret());
        return baseClientDetails;
    }

    @Autowired
    public void setOauthClientDetailsDao(OauthClientDetailsDao oauthClientDetailsDao) {
        this.oauthClientDetailsDao = oauthClientDetailsDao;
    }
}
