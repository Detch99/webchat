package com.detch.auth.controller;

import com.detch.auth.model.RegistrationDTO;
import com.detch.auth.model.RegistrationSuccessResponse;
import com.detch.auth.model.UserDTO;
import com.detch.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/registration")
    public String showRegistrationForm() {
        return "registration";
    }

    @PostMapping(value = "/registration")
    public ResponseEntity<RegistrationSuccessResponse> registerUser(@Valid RegistrationDTO registrationDTO) {
        UserDTO user = userService.registerUserAccount(registrationDTO);
        RegistrationSuccessResponse response = new RegistrationSuccessResponse(user);
        response.setStatus(HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
