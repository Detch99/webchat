package com.detch.auth.controller;

import com.detch.auth.exceptions.UserAlreadyExistsException;
import com.detch.auth.model.RegistrationFailResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class RegistrationErrorHandler {

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<RegistrationFailResponse> handleUserAlreadyExistException() {
        Map<String, List<String>> fieldErrors = Collections.singletonMap(
                "username",
                List.of("User with such username already exists")
        );
        RegistrationFailResponse response = new RegistrationFailResponse(fieldErrors);
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<RegistrationFailResponse> handleValidationException(BindException exception) {
        Map<String, List<String>> fieldErrors = new HashMap<>();
        exception.getFieldErrors().forEach(error -> {
            String field = error.getField();
            if (fieldErrors.containsKey(field)) {
                fieldErrors.get(field).add(error.getDefaultMessage());
            } else {
                List<String> errors = new ArrayList<>();
                errors.add(error.getDefaultMessage());
                fieldErrors.put(field, errors);
            }
        });
        RegistrationFailResponse response = new RegistrationFailResponse(fieldErrors);
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
