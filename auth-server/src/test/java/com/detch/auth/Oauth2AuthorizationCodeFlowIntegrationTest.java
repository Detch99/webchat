package com.detch.auth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.http.HttpSession;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AuthServer.class)
@AutoConfigureMockMvc
public class Oauth2AuthorizationCodeFlowIntegrationTest {

    private static final String CLIENT_ID = "clientId";
    private static final String AUTHORIZED_GRANT_TYPE = "authorization_code";
    private static final String REDIRECT_URI = "http://example.com";
    private static final String USERNAME = "user";
    private static final String PASSWORD = "secret";
    private static final String AUTHORIZE_URL = String.format( "/oauth/authorize?client_id=%s&response_type=code&redirect_uri=%s", CLIENT_ID, REDIRECT_URI );

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void testLoginWithBadCredentials() throws Exception {
        mvc.perform(formLogin().user("bad").password("bad"))
           .andExpect(redirectedUrl("/login?error"));
    }

    @Test
    public void testSuccessfulLogin() throws Exception {
        mvc.perform(formLogin().user(USERNAME).password(PASSWORD))
           .andExpect(redirectedUrl("/"));
    }

    @Test
    public void testAuthorizeWhenLoggedOut() throws Exception {
        mvc.perform(get(AUTHORIZE_URL)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
           .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    public void testTokenObtainingWithIncorrectAuthorizationCode() throws Exception {
        mvc.perform(post("/oauth/token").with(httpBasic(CLIENT_ID, PASSWORD))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("client_id", CLIENT_ID)
                .param("grant_type", AUTHORIZED_GRANT_TYPE)
                .param("code", "bad")
                .param("redirect_uri", REDIRECT_URI))
           .andExpect(status().is4xxClientError());
    }

    @Test
    public void testTokenObtainingWithFailedBasicAuthorization() throws Exception {
        mvc.perform(post("/oauth/token").with(httpBasic("bad", "bad"))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("client_id", CLIENT_ID)
                .param("grant_type", AUTHORIZED_GRANT_TYPE)
                .param("code", getAuthorizationCode())
                .param("redirect_uri", REDIRECT_URI))
           .andExpect(status().is4xxClientError());
    }

    @Test
    public void testSuccessfulObtainToken() throws Exception {
        mvc.perform(post("/oauth/token").with(httpBasic(CLIENT_ID, PASSWORD))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("client_id", CLIENT_ID)
                .param("grant_type", AUTHORIZED_GRANT_TYPE)
                .param("code", getAuthorizationCode())
                .param("redirect_uri", REDIRECT_URI))
           .andExpect(status().isOk());
    }

    @Test
    public void testForInabilityToReceiveTwoTokensByOneAuthorizationCode() throws Exception {
        String authorizationCode = getAuthorizationCode();

        mvc.perform(post("/oauth/token").with(httpBasic(CLIENT_ID, PASSWORD))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("client_id", CLIENT_ID)
                .param("grant_type", AUTHORIZED_GRANT_TYPE)
                .param("code", authorizationCode)
                .param("redirect_uri", REDIRECT_URI))
           .andExpect(status().isOk());

        mvc.perform(post("/oauth/token").with(httpBasic(CLIENT_ID, PASSWORD))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("client_id", CLIENT_ID)
                .param("grant_type", AUTHORIZED_GRANT_TYPE)
                .param("code", authorizationCode)
                .param("redirect_uri", REDIRECT_URI))
           .andExpect(status().is4xxClientError());
    }

    private String getAuthorizationCode() throws Exception {
        MvcResult authorizationResult = mvc
                .perform(formLogin().user(USERNAME).password(PASSWORD))
                .andReturn();

        HttpSession authSession = authorizationResult.getRequest().getSession();
        Assert.assertNotNull(authSession);

        String redirectUrl = mvc
                .perform(get(AUTHORIZE_URL)
                        .session((MockHttpSession) authSession))
                .andReturn()
                .getResponse()
                .getRedirectedUrl();

        Assert.assertNotNull(redirectUrl);
        String authorizationCode = redirectUrl.split("code=")[1];
        Assert.assertNotNull(authorizationCode);
        return authorizationCode;
    }
}
