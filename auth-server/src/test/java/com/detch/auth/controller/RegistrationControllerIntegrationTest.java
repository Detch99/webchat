package com.detch.auth.controller;

import com.detch.auth.AuthServer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AuthServer.class)
@AutoConfigureMockMvc
public class RegistrationControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testSuccessRegistration() throws Exception {
        mvc.perform(post("/registration")
                .param("username", "username")
                .param("password", "password")
                .param("matchingPassword", "password"))
            .andExpect(status().isOk());
    }

    @Test
    public void testFailedRegistration_WhereUsernameAlreadyExists() throws Exception {
        mvc.perform(post("/registration")
                .param("username", "username2")
                .param("password", "password")
                .param("matchingPassword", "password"))
            .andExpect(status().isOk());

        mvc.perform(post("/registration")
                .param("username", "username2")
                .param("password", "password")
                .param("matchingPassword", "password"))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testFiledRegistration_WherePasswordsDontMatch() throws Exception {
        mvc.perform(post("/registration")
                .param("username", "username")
                .param("password", "password")
                .param("matchingPassword", "otherPassword"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testFailedRegistration_EmptyUsername() throws Exception {
        mvc.perform(post("/registration")
                .param("username", "")
                .param("password", "password")
                .param("matchingPassword", "password"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testFailedRegistration_EmptyRequest() throws Exception {
        mvc.perform(post("/registration")).andExpect(status().isBadRequest());
    }
}
