package com.detch.auth.service;

import com.detch.auth.dao.OauthClientDetailsDao;
import com.detch.auth.model.OauthClientDetails;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;

import java.util.Optional;

import static org.springframework.util.StringUtils.commaDelimitedListToSet;

@RunWith(MockitoJUnitRunner.class)
public class ClientDetailsServiceImplTest {

    @Mock
    private OauthClientDetailsDao mockOauthClientDetailsDao;

    @Test
    public void testSuccessfulLoadClientByClientId() {
        ClientDetailsServiceImpl clientDetailsService = new ClientDetailsServiceImpl();
        OauthClientDetails oauthClientDetails = new OauthClientDetails();

        clientDetailsService.setOauthClientDetailsDao(mockOauthClientDetailsDao);

        oauthClientDetails.setClientId("clientId");
        oauthClientDetails.setClientSecret("secret");
        oauthClientDetails.setScope("read");
        oauthClientDetails.setAuthorizedGrantTypes("authorization_code");
        oauthClientDetails.setRedirectUris("http://example.com");
        oauthClientDetails.setAutoApproveScopes("true");

        BaseClientDetails baseClientDetails = new BaseClientDetails();

        baseClientDetails.setClientId(oauthClientDetails.getClientId());
        baseClientDetails.setScope(commaDelimitedListToSet(oauthClientDetails.getScope()));
        baseClientDetails.setAuthorizedGrantTypes(commaDelimitedListToSet(oauthClientDetails.getAuthorizedGrantTypes()));
        baseClientDetails.setRegisteredRedirectUri(commaDelimitedListToSet(oauthClientDetails.getRedirectUris()));
        baseClientDetails.setAutoApproveScopes(commaDelimitedListToSet(oauthClientDetails.getAutoApproveScopes()));
        baseClientDetails.setClientSecret(oauthClientDetails.getClientSecret());

        Mockito.when(mockOauthClientDetailsDao.findById("clientId"))
                .thenReturn(Optional.of(oauthClientDetails));

        Assert.assertEquals(baseClientDetails, clientDetailsService.loadClientByClientId("clientId"));
    }

    @Test(expected = ClientRegistrationException.class)
    public void testWhenClientDoesntExist() {
        ClientDetailsServiceImpl clientDetailsService = new ClientDetailsServiceImpl();

        clientDetailsService.setOauthClientDetailsDao(mockOauthClientDetailsDao);
        clientDetailsService.loadClientByClientId("bad");
    }
}
