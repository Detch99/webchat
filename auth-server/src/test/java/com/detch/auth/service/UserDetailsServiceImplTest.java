package com.detch.auth.service;

import com.detch.auth.dao.UserDao;
import com.detch.auth.model.Role;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.HashSet;
import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailsServiceImplTest {

    @Mock
    private UserDao userDao;

    @Test
    public void testSuccessfulLoadUserByUsername() {
        UserDetailsServiceImpl userDetailsService = new UserDetailsServiceImpl();
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        com.detch.auth.model.User user = new com.detch.auth.model.User();
        Role role = new Role();
        Set<Role> roles = new HashSet<>();

        userDetailsService.setUserDao(userDao);

        role.setId(1);
        role.setName("user");

        roles.add(role);
        grantedAuthorities.add(new SimpleGrantedAuthority("user"));

        user.setId(1);
        user.setUsername("username");
        user.setPassword("password");
        user.setRoles(roles);
        user.setUuid("uuid");

        User userDetails = new User(
                "username",
                "password",
                grantedAuthorities
        );

        Mockito.when(userDao.findByUsername("username"))
                .thenReturn(user);

        Assert.assertEquals(userDetails, userDetailsService.loadUserByUsername("username"));
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testWhenClientDoesntExist() {
        UserDetailsServiceImpl userDetailsService = new UserDetailsServiceImpl();

        userDetailsService.setUserDao(userDao);
        userDetailsService.loadUserByUsername("bad");
    }
}
