-- password: "secret"
insert into oauth_client_details values ('clientId', '$2y$11$PapORf9rLDD/uvP0AvhfTuS355muFXKJWmtKOg/hphlHXM3NuQDN6', 'read', 'authorization_code', 'http://example.com', 1000, 1000, 'read, write');
insert into users values (1, 'uuid', 'user', '$2y$11$PapORf9rLDD/uvP0AvhfTuS355muFXKJWmtKOg/hphlHXM3NuQDN6');
insert into roles values (1, 'ROLE_USER');
insert into user_roles values (1, 1);